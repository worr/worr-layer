;;; Commentary:

;; Defines some functions that ease our configuration

;;; Code:
(defun worr/install-pyreqs ()
  "Install python requirements when we enter a venv"
  (setq anaconda-mode-process
        (start-pythonic :process anaconda-mode-process-name
                        :buffer anaconda-mode-process-buffer
                        :args (list "-c from setuptools.command import easy_install;"
                                    "easy_install.main(['isort', 'black', 'mypy',"
                                    " 'anaconda-mode', 'jedi', 'autoflake',"
                                    "'flake8'])"))))

(defun worr/auto-wrap-column (cols)
  "Sets fill-column and then enables wrapping"
  (setq-default fill-column cols)
  (auto-fill-mode))

(defun worr/pulse-line (&rest _)
  "Pulse the current line."
  (pulse-momentary-highlight-one-line (point)))

(defun worr/setup-ligatures ()
  "Turns on ligature support. Did I even want this"
  ;; Enable the www ligature in every possible major mode
  (ligature-set-ligatures 't '("www"))

  ;; Enable ligatures in programming modes
  (ligature-set-ligatures 'prog-mode '("www" "**" "***" "**/" "*>" "*/" "\\\\" "\\\\\\" "{-" "::"
                                       ":::" ":=" "!!" "!=" "!==" "-}" "----" "-->" "->" "->>"
                                       "-<" "-<<" "-~" "#{" "#[" "##" "###" "####" "#(" "#?" "#_"
                                       "#_(" ".-" ".=" ".." "..<" "..." "?=" "??" ";;" "/*" "/**"
                                       "/=" "/==" "/>" "//" "///" "&&" "||" "||=" "|=" "|>" "^=" "$>"
                                       "++" "+++" "+>" "=:=" "==" "===" "==>" "=>" "=>>" "<="
                                       "=<<" "=/=" ">-" ">=" ">=>" ">>" ">>-" ">>=" ">>>" "<*"
                                       "<*>" "<|" "<|>" "<$" "<$>" "<!--" "<-" "<--" "<->" "<+"
                                       "<+>" "<=" "<==" "<=>" "<=<" "<>" "<<" "<<-" "<<=" "<<<"
                                       "<~" "<~~" "</" "</>" "~@" "~-" "~>" "~~" "~~>" "%%")))

(defun worr/change-undo-tree-history-dir ()
  "Changes the directory for undo-tree files to my user-dir."
  (let ((undo-tree-dir (f-join user-emacs-directory "undo-tree")))
    (ignore-error file-already-exists
      (make-directory undo-tree-dir))
    (setq undo-tree-history-directory-alist
          `(("." . ,undo-tree-dir)))))
