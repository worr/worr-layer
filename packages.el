;;; packages.el --- worr layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2017 Sylvain Benner & Contributors
;;
;; Author: William Orr <will@worrbase.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;;; Commentary:

;; See the Spacemacs documentation and FAQs for instructions on how to implement
;; a new layer:
;;
;;   SPC h SPC layers RET
;;
;;
;; Briefly, each package to be installed or configured by this layer should be
;; added to `worr-packages'. Then, for each package PACKAGE:
;;
;; - If PACKAGE is not referenced by any other Spacemacs layer, define a
;;   function `worr/init-PACKAGE' to load and initialize the package.

;; - Otherwise, PACKAGE is already referenced by another Spacemacs layer, so
;;   define the functions `worr/pre-init-PACKAGE' and/or
;;   `worr/post-init-PACKAGE' to customize the package as it is loaded.

;;; Code:

(defconst worr-packages
  '(
    exec-path-from-shell
    flycheck-mypy
    (ligature
     :location (recipe
                :fetcher github
                :repo "mickeynp/ligature.el"))
    visual-fill-column
    )
  "The list of Lisp packages required by the worr layer.

Each entry is either:

1. A symbol, which is interpreted as a package to be installed, or

2. A list of the form (PACKAGE KEYS...), where PACKAGE is the
    name of the package to be installed or loaded, and KEYS are
    any number of keyword-value-pairs.

    The following keys are accepted:

    - :excluded (t or nil): Prevent the package from being loaded
      if value is non-nil

    - :location: Specify a custom installation location.
      The following values are legal:

      - The symbol `elpa' (default) means PACKAGE will be
        installed using the Emacs package manager.

      - The symbol `local' directs Spacemacs to load the file at
        `./local/PACKAGE/PACKAGE.el'

      - A list beginning with the symbol `recipe' is a melpa
        recipe.  See: https://github.com/milkypostman/melpa#recipe-format")

(defun worr/init-flycheck-mypy ()
  (use-package flycheck-mypy
    :defer t))

;; Don't defer this since it's needed for startup
(defun worr/init-exec-path-from-shell ()
  (use-package exec-path-from-shell
    :init
    (setq exec-path-from-shell-variables
          '("ENV" "LANG" "LC_ALL" "PATH" "MANPATH"))
    (when (memq window-system '(mac ns x))
      (exec-path-from-shell-initialize))))

(defun worr/init-visual-fill-column ()
  (use-package visual-fill-column
    :defer t))

(defun worr/init-ligature ()
  (use-package ligature
    :config
    (if (eq system-type 'darwin)
        (mac-auto-operator-composition-mode)
      (add-hook 'ligature-mode-hook 'worr/setup-ligatures)
      (global-ligature-mode))))
