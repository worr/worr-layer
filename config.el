;;; Commentary:

;; Python defaults:
;; - enable flycheck-mypy
;; - install required packages when entering a virtualenv
;; - adopt indent style in file
;;
;; C defaults:
;; - bsd style
;; - adopt indent style in file
;;
;; Markdown defaults:
;; - wrap at 80 chars
;;
;; Org defaults:
;; - wrap at 80 chars
;;
;; Sh defaults:
;; - adopt indent style in file
;;
;; EWW defaults:
;; - reload with `g'

;;; Code:

;; tabs should always be 4 characters wide
(setq-default tab-width 4)

;; OpenBSD knf style from https://raw.githubusercontent.com/hogand/openbsd-knf-emacs/master/openbsd-knf-style.el
;; This function is from nicm@
(defun openbsd-knf-space-indent (langelem)
  "Indents either 4 spaces or none.

This function is from nicm@ and also in gsoares@'s config.
Most useful with c-offset-alist entries that are lists such as
arglist-cont-nonempty"
  (save-excursion
    (goto-char (cdr langelem))
    (setq syntax (car (car (c-guess-basic-syntax))))
    (while (or (eq syntax 'arglist-intro)
	       (or (eq syntax 'arglist-cont)
		   (eq syntax 'arglist-cont-nonempty)))
      (forward-line -1)
      (setq syntax (car (car (c-guess-basic-syntax)))))
    (beginning-of-line)
    (re-search-forward "[^ \t]" (c-point 'eol))
    (goto-char (+ (match-beginning 0) 4))
    (vector (current-column))))

;; OpenBSD KNF that doug@ uses.
(defconst openbsd-knf-style
  '(
    ;; Debugging
    ;; (c-echo-syntactic-information-p . t) ;; display C-c C-s when hit 'tab'
    ;;
    ;; General settings that should be enabled in c-mode
    (indent-tabs-mode . t)      ;; use tabs whenever feasible
    (fill-column . 80)          ;; Assume KNF tries to maintain 80 char lines
    (show-trailing-whitespace . t)  ;; KNF says to not have trailing WS
    (tab-width . 8)             ;; When displaying literal tab, show 8 spaces
    ;; c-mode
    (c-progress-interval . 1)   ;; display progress meter during long indents
    (c-basic-offset . 8)        ;; KNF uses 8 space tabs
    (c-comment-only-line-offset . 0)  ;; don't indent comments extra
    (c-backspace-function . delete-backward-char)
    (c-delete-function . delete-char)
    (c-syntactic-indentation-in-macros . t) ;; indent inside macro defs
    (c-tab-always-indent . t)  ;; indent line: Use C-q 'tab' to insert literal
    (c-continued-statement-offset 0)
    ;; (c-electric-flag . nil)   ;; if you don't like auto-indent
    (c-electric-continued-statement . t)
    ;;
    (c-indent-comments-syntactically-p . nil)
    ;;
    ;; Offsets for the various c-mode symbols.  Offsets are sometimes based
    ;; upon other offsets.  For instance, arglist-intro is the 1st argument
    ;; line.  If you define arglist-cont, it uses arglist-intro plus that.
    ;; c-echo-syntactic-information-p is your friend when debugging indents.
    ;;
    ;; [N] means absolute column.  All the others are relative.
    ;;  0 = no extra indentation.  For literal column 0, use [0]
    ;;  N = extra N spaces.  For literal column N, use [N]
    ;; ++ = c-basic-offset * 2
    ;; -- = c-basic-offset * -2
    ;;  + = c-basic-offset * 1
    ;;  - = c-basic-offset * -1
    ;;  * = c-basic-offset * 0.5
    ;;  / = c-basic-offset * -0.5
    (c-offsets-alist . (
			;; Literal symbols
			(func-decl-cont . 0)        ; C++ style func mod
			(block-open . 0)            ; '{' for block
			(label . [1])               ; goto label in column 1
			(comment-intro . 0)         ; C comment
			(cpp-macro . [0])           ; #define in column 0
			;; Multiline macro symbols
			(cpp-define-intro . [0])    ; first list = column 0
			(cpp-macro-cont . +)        ; add'l lines in macro
			;; Function symbols
			(defun-open . 0)            ; '{' alone for func
			(defun-close . 0)           ; '}' alone for func
			(defun-block-intro . +)     ; first line of func
			(topmost-intro . 0)         ; outermost part
			(topmost-intro-cont . 0)    ; outermost part cont
			(statement . 0)             ; func stmt (already off)
			;; XXX statement-cont should be 4 unless
			;; it is part of a macro, then 8.
			(statement-cont . *)        ; continue stmt
			;; Class symbols.  XXX Should add support since there
			;; is a little C++ in the tree (GNU)
			;; Java
			;; K&R
			(knr-argdecl-intro . +)     ; rare K&R (from KNF)
			(knr-argdecl . 0)           ; add'l indent for rest
			;; Conditional construct symbols
			(block-close . 0)           ; '}' for block
			(statement-block-intro . +) ; stmt in loop/cond
			(substatement . +)          ; non-braced stmt if()
			(substatement-open . 0)     ; '{' in loop/cond
			(substatement-label . [1])  ; goto label in loop/cond
			(do-while-closure . 0)      ; 'while' alone in 'do'
			(else-clause . 0)           ; 'else' when not nested
			;; Brace list symbols
			(brace-list-close . 0)      ; enum/agg list close
			(brace-list-intro . +)      ; 1st line of enum/agg
			(brace-list-entry . 0)      ; add'l indent for entries
			(brace-list-open . 0)       ; enum/agg init open
			;; Switch statement symbols
			(statement-case-open . +)   ; '{' in case
			(statement-case-intro . +)  ; 1st line in case stmt
			(case-label . 0)            ; case label in switch
			;; Paren list symbols
			;; XXX This is typically a list so need to handle it
			;; differently from the rest.  Emacs adds the indents.
			(arglist-intro . openbsd-knf-space-indent) ; 1st line
			(arglist-cont . openbsd-knf-space-indent)
			(arglist-cont-nonempty . openbsd-knf-space-indent)
			(arglist-close . 0)         ; ')' alone
			;; External scope symbols
			(extern-lang-open . [0])    ; '{' alone in 'extern C'
			(extern-lang-close . [0])   ; '}' alone in 'extern C'
			(inextern-lang . +)         ; lines inside 'extern C'
			;; Statement block
			(inexpr-statement . +)))    ; gcc extension stmt expr
    ;; If not specified, the default is "before after".  All variables are
    ;; defined here.
    (c-hanging-braces-alist . (
			       ;; All variables
			       (defun-open before after)  ; function, enum
			       (defun-close before after) ; function
			       (class-open after) ; struct too
			       (class-close before after)
			       (inline-open after)
			       (inline-close before after)
			       (block-open after)
                               (block-close . c-snug-do-while)
			       (statement-cont after)
			       (substatement-open after)
			       (statement-case-open before after)
			       (brace-list-open after)
			       (brace-list-close before close)
			       (brace-list-intro after)
			       (brace-entry-open after)
			       (extern-lang-open after)
			       (extern-lang-close before after)
			       (namespace-open after)           ;; C++
			       (namespace-close before afetr)   ;; C++
			       (module-open after)              ;; CORBA
			       (module-close before after)      ;; CORBA
			       (composition-open after)         ;; CORBA
			       (composition-close before after) ;; CORBA
			       (inexpr-class-open after)
			       (inexpr-class-close before after)
			       (arglist-cont-nonempty before after)))
    ;; Whether to auto-insert newline before/after colon
    (c-hanging-colons-alist . ((case-label after)
                               (label after)
                               (access-label after)  ;; C++
                               (member-init-intro before)
                               (inher-intro)))
    ;; Whether to insert newlines after ';' or ','
    (c-hanging-semi&comma-criteria . (
				      ;; supress newline when next line non-blank
				      c-semi&comma-no-newlines-before-nonblanks
				      ;; suppress newline in paren (for loop etc)
				      c-semi&comma-inside-parenlist
				      ;; supress newline for one liner
				      c-semi&comma-no-newlines-for-oneline-inliners))
    ;; When autonewline mode is enabled, clean up some extra newlines
    (c-cleanup-list . (brace-else-brace    ; } else {
                       brace-elseif-brace  ; } else if {
                       brace-catch-brace   ; } catch (...) {
                       ;; empty-defun-braces ; {} instead of multiple lines
                       defun-close-semi    ; struct: no \n between '}' and ';'
                       list-close-comma    ; remove final comma
                       scope-operator
		       ;; space-before-funcall ; GNU standard
		       ;; compact-empty-funcall ; another GNU standard
		       ;; comment-close-slash ; term comment with slash
		       ))
    ))

;; Python config
(add-hook 'python-mode-hook
          (lambda ()
            (flycheck-add-next-checker
             'python-flake8
             'python-mypy)))

(add-hook 'pyvenv-post-activate-hooks 'worr/install-pyreqs)

;; C config
(add-hook 'c-mode-common-hook
          (lambda ()
            (c-add-style "OpenBSD" openbsd-knf-style)
            (setq-default c-default-style "OpenBSD")))

;; Markdown config
(add-hook 'markdown-mode-hook
          (lambda ()
            (worr/auto-wrap-column 80)
            (setq-default markdown-list-indent-width 2)))

;; Org-mode config
(add-hook 'org-mode-hook
          (lambda () (worr/auto-wrap-column 80)))

;; Rust config
(add-hook 'rust-mode-hook
          (lambda ()
            (if (eq system-type 'berkeley-unix)
                (progn
                  (setenv "OPENSSL_LIB_DIR" "/usr/local/lib/eopenssl11")
                  (setenv "OPENSSL_INCLUDE_DIR" "/usr/local/include/eopenssl11/")))))

;; shell-script-mode config
(add-hook 'sh-mode-hook
          (lambda ()
            (setq-default sh-basic-offset 4
                          indent-tabs-mode t)))

;; go config
(add-hook 'go-mode-hook
          (lambda ()
            (setq-default go-tab-width tab-width
                          lsp-go-use-gofumpt t
                          lsp-go-build-flags ["-tags" "integration"]
                          flycheck-golangci-lint-fast t)))

;; web browser
(add-hook 'eww-mode-hook
          (lambda ()
            (spacemacs/set-leader-keys-for-major-mode 'eww-mode
              "g" 'eww-reload)))

;; Visual line mode (wrapped text for reading email)
(add-hook 'visual-line-mode-hook #'visual-fill-column-mode)

;; Replace woman with man on K
(setq-default evil-lookup-func #'lazy-helm/helm-man-woman)

;; Blink on search
(dolist (command '(evil-scroll-page-up
                   evil-scroll-page-down
                   evil-window-up
                   evil-window-down
                   evil-window-left
                   evil-window-right
                   other-window
                   winum-select-window-by-number
                   evil-next-match
                   evil-ex-search-next
                   evil-previous-match
                   evil-ex-search-previous))
  (advice-add command :after #'worr/pulse-line))

;; Display a time in our modeline
(setq-default display-time-24hr-format t)
(setq-default display-time-default-load-average nil)
(display-time-mode 1)

;; do not use fd or jk or whatever to do stupid escaping
(setq-default evil-escape-key-sequence nil)

;; Use GNU ls
(if (eq system-type 'darwin)
  (setq-default insert-directory-program "/opt/homebrew/bin/gls")
  (setq-default insert-directory-program "/usr/local/bin/gls"))

;; Use ectags
(setq-default projectile-tags-command "/usr/local/bin/uctags -Re -f \"%s\" %s \"%s\"")

;; Set treemacs width
(setq-default treemacs-width 25)

;; Put undo-tree files in a subdir under .emacs.d
(add-hook 'undo-tree-mode-hook #'worr/change-undo-tree-history-dir)

;; Do not show closed issues
(setq-default forge-topic-list-limit '(60 . -5))

;; Add work to the list of supported forges
(add-hook 'magit-mode-hook
          (lambda ()
            (if (not (assoc "github.mpi-internal.com" forge-alist))
                (push '("github.mpi-internal.com" "github.mpi-internal.com/api/v3" "github.mpi-internal.com" forge-github-repository) forge-alist))))

(setq treesit-language-source-alist
   '((bash "https://github.com/tree-sitter/tree-sitter-bash")
     (cmake "https://github.com/uyha/tree-sitter-cmake")
     (css "https://github.com/tree-sitter/tree-sitter-css")
     (elisp "https://github.com/Wilfred/tree-sitter-elisp")
     (go "https://github.com/tree-sitter/tree-sitter-go")
     (gomod "https://github.com/camdencheek/tree-sitter-go-mod")
     (html "https://github.com/tree-sitter/tree-sitter-html")
     (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
     (json "https://github.com/tree-sitter/tree-sitter-json")
     (make "https://github.com/alemuller/tree-sitter-make")
     (markdown "https://github.com/ikatyang/tree-sitter-markdown")
     (python "https://github.com/tree-sitter/tree-sitter-python")
     (rust "https://github.com/tree-sitter/tree-sitter-rust")
     (toml "https://github.com/tree-sitter/tree-sitter-toml")
     (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
     (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
     (yaml "https://github.com/ikatyang/tree-sitter-yaml")))
(setq major-mode-remap-alist
      '((bash-mode . bash-ts-mode)
        (css-mode . css-ts-mode)
        ; (go-mode . go-ts-mode) breaks lsp-mode
        (html-mode . html-ts-mode)
        (js2-mode . js-ts-mode)
        (json-mode . json-ts-mode)
        (python-mode . python-ts-mode)
        ; (rust-mode . rust-ts-mode) handled by rustic-mode
        (toml-mode . toml-ts-mode)
        (typescript-mode . typescript-ts-mode)
        (typescript-tsx-mode . tsx-ts-mode)
        (yaml-mode . yaml-ts-mode)))

;; Don't look in mocks folder
(setq helm-ag-command-option "--glob=!mocks")
